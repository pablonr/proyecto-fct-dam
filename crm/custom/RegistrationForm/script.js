let app = new Vue({
    el: '#app',
    data: {
        entryPoint: 'https://proypnr.activarunner.com/index.php?entryPoint=external_form_controller',
        courses: [],
        form: {
            birthdate: '',
            city: '',
            country: '',
            course: '',
            dni: '',
            email: '',
            firstName: '',
            lastName: '',
            observations: '',
            parentEmail: '',
            parentFirstName: '',
            parentLastName: '',
            parentPhone: '',
            parentalConsent: false,
            phoneNumber: '',
            postalCode: '',
            state: '',
            street: ''
        }
    },
    methods: {
        getCourses: function() {
            $.ajax({
                url: `${this.entryPoint}&action=get_courses`,
                type: 'GET',
                dataType: 'json',
            }).done((response) => {
                response.sort()
                this.courses = response
            })
        },

        submitForm: function(e) {
            e.preventDefault()
            let data = {}
            Object.keys(this.form).forEach(key => {
                data[key] = this.form[key]
            })
            data.action = 'submit'
            $.ajax({
                url: this.entryPoint,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done((response) => {
                if(response) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Formulario enviado correctamente',
                        showConfirmButton: false,
                        timer: 2000
                    }).then(() => {
                        location.reload()
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Ha ocurrido algun error al registrar el formulario',
                        showConfirmButton: false,
                        timer: 2000
                    })
                }
            })
        }
    },
    mounted: function() {
        this.getCourses()
    }
})