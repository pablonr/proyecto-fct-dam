<?php

if($_GET['action'] == 'get_courses') {

    $query = "SELECT dc.id, dc.name FROM dam_courses dc WHERE dc.deleted = 0 ORDER BY name";
    $result = $GLOBALS['db']->query($query);
    $courses = array();

    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $courses[] = $row;
        }
    }

    echo json_encode($courses);
    exit();
}

if($_POST['action'] == "submit") {

    $requestId = create_guid();

    $insert = "INSERT INTO dam_student_requests (id, date_entered, date_modified, modified_user_id, created_by, deleted, first_name, last_name, phone_home, primary_address_street, primary_address_city, primary_address_state, primary_address_postalcode, primary_address_country) 
    VALUES ('$requestId', NOW(), NOW(), '1', '1', 0, '$_POST[firstName]', '$_POST[lastName]', '$_POST[phoneNumber]', '$_POST[street]', '$_POST[city]', '$_POST[state]', '$_POST[postalCode]', '$_POST[country]')";

    $insertCSTM = "INSERT INTO dam_student_requests_cstm (id_c, student_dni_c, birthdate_c, parental_consent_c, parents_first_name_c, parents_last_name_c, parents_phone_c, parents_email_c, request_status_c, request_origin_c, repeat_course_c, observations_c) VALUES ('$requestId', '$_POST[dni]', '$_POST[birthdate]', $_POST[parentalConsent], '$_POST[parentFirstName]', '$_POST[parentLastName]', '$_POST[parentPhone]', '$_POST[parentEmail]', 'pending', 'web', 0, '$_POST[observations]')";

    $relId = create_guid();
    $insertCourseRel = "INSERT INTO dam_courses_dam_student_requests_1_c VALUES ('$relId', NOW(), 0, '$_POST[course]', '$requestId')";

    $emailId = create_guid();
    $email = $_POST['email'];
    $emailUpper = strtoupper($email);
    $insertEmail = "INSERT INTO email_addresses VALUES ('$emailId', '$email', '$emailUpper', 0, 0, 'not-opt-in', NULL, NULL, NULL, NULL, NOW(), NOW(), 0)";

    $emailRelId = create_guid();
    $insertEmailRel = "INSERT INTO email_addr_bean_rel VALUES ('$emailRelId', '$emailId', '$requestId', 'dam_student_requests', 1, 0, NOW(), NOW(), 0)";

    $resultInsert = $GLOBALS['db']->query($insert);
    $resultInsertCSTM = $GLOBALS['db']->query($insertCSTM);
    $resultInsertRel = $GLOBALS['db']->query($insertCourseRel);
    $resultInsertEmail = $GLOBALS['db']->query($insertEmail);
    $resultInsertEmailRel = $GLOBALS['db']->query($insertEmailRel);

    echo json_encode(array('error' => !($resultInsert && $resultInsertCSTM && $resultInsertRel && $resultInsertEmail && $resultInsertEmailRel)));
    exit();

}

?>