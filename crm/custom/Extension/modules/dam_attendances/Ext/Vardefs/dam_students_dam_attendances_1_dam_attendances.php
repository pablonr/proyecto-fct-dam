<?php
// created: 2022-05-30 19:21:55
$dictionary["dam_attendances"]["fields"]["dam_students_dam_attendances_1"] = array (
  'name' => 'dam_students_dam_attendances_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_attendances_1',
  'source' => 'non-db',
  'module' => 'dam_students',
  'bean_name' => 'dam_students',
  'vname' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_STUDENTS_TITLE',
  'id_name' => 'dam_students_dam_attendances_1dam_students_ida',
);
$dictionary["dam_attendances"]["fields"]["dam_students_dam_attendances_1_name"] = array (
  'name' => 'dam_students_dam_attendances_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_STUDENTS_TITLE',
  'save' => true,
  'id_name' => 'dam_students_dam_attendances_1dam_students_ida',
  'link' => 'dam_students_dam_attendances_1',
  'table' => 'dam_students',
  'module' => 'dam_students',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["dam_attendances"]["fields"]["dam_students_dam_attendances_1dam_students_ida"] = array (
  'name' => 'dam_students_dam_attendances_1dam_students_ida',
  'type' => 'link',
  'relationship' => 'dam_students_dam_attendances_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE',
);
