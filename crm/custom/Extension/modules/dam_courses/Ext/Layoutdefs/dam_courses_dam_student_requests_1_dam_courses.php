<?php
 // created: 2022-04-20 15:47:00
$layout_defs["dam_courses"]["subpanel_setup"]['dam_courses_dam_student_requests_1'] = array (
  'order' => 100,
  'module' => 'dam_student_requests',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
  'get_subpanel_data' => 'dam_courses_dam_student_requests_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
