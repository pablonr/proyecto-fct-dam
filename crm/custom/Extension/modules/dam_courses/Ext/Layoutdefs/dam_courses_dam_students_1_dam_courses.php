<?php
 // created: 2022-04-22 11:23:31
$layout_defs["dam_courses"]["subpanel_setup"]['dam_courses_dam_students_1'] = array (
  'order' => 100,
  'module' => 'dam_students',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE',
  'get_subpanel_data' => 'dam_courses_dam_students_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
