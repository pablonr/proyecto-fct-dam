<?php
// created: 2022-05-30 17:30:09
$mod_strings['LBL_ASSIGNED_TO_NAME'] = 'Profesor asignado';
$mod_strings['LBL_ASSIGNED_TO'] = 'Profesor asignado';
$mod_strings['LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE'] = 'Cursando actualmente';
$mod_strings['LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_STUDENTS_TITLE'] = 'Alumnos que han cursado';
$mod_strings['LBL_DISCORD_ID'] = 'Id rol Discord';
