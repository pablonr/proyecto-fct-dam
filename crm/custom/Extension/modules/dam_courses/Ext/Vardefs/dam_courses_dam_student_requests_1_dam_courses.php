<?php
// created: 2022-04-20 15:47:00
$dictionary["dam_courses"]["fields"]["dam_courses_dam_student_requests_1"] = array (
  'name' => 'dam_courses_dam_student_requests_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'module' => 'dam_student_requests',
  'bean_name' => 'dam_student_requests',
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
);
