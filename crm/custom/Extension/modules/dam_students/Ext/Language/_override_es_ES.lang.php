<?php
// created: 2022-05-30 19:41:56
$mod_strings['LBL_PARENTS_EMAIL'] = 'Correo electrónico (Padre/Madre/Tutor)';
$mod_strings['LBL_PARENTS_FIRST_NAME'] = 'Nombre (Padre/Madre/Tutor)';
$mod_strings['LBL_OBSERVATIONS'] = 'Observaciones';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento';
$mod_strings['LBL_PARENTS_PHONE'] = 'Numero de teléfono (Padre/Madre/Tutor)';
$mod_strings['LBL_PARENTS_LAST_NAME'] = 'Apellidos (Padre/Madre/Tutor)';
$mod_strings['LBL_STUDENT_DNI'] = 'DNI';
$mod_strings['LBL_PARENTAL_CONSENT'] = 'Consentimiento de los padres';
$mod_strings['LBL_CONTACT_INFORMATION'] = 'Datos del alumno';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'Datos familia';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'Otros datos';
$mod_strings['LNK_NEW_RECORD'] = 'Crear Alumnos';
$mod_strings['LNK_LIST'] = 'Vista Alumnos';
$mod_strings['LNK_IMPORT_VCARD'] = 'Crear Desde vCard';
$mod_strings['LNK_IMPORT_DAM_STUDENTS'] = 'Importar Alumnos';
$mod_strings['LBL_LIST_FORM_TITLE'] = 'Alumnos Lista';
$mod_strings['LBL_SEARCH_FORM_TITLE'] = 'Búsqueda Alumnos';
$mod_strings['LBL_HOMEPAGE_TITLE'] = 'Mi Alumnoss';
$mod_strings['LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE'] = 'Cursando actualmente';
$mod_strings['LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_COURSES_TITLE'] = 'Ha cursado';
$mod_strings['LBL_DISCORD_ID'] = 'discord id';
$mod_strings['LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE'] = 'Asistencias';
