<?php
// created: 2022-05-30 19:21:55
$dictionary["dam_students"]["fields"]["dam_students_dam_attendances_1"] = array (
  'name' => 'dam_students_dam_attendances_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_attendances_1',
  'source' => 'non-db',
  'module' => 'dam_attendances',
  'bean_name' => 'dam_attendances',
  'side' => 'right',
  'vname' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE',
);
