<?php
// created: 2022-04-22 11:23:31
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1"] = array (
  'name' => 'dam_courses_dam_students_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_students_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
  'id_name' => 'dam_courses_dam_students_1dam_courses_ida',
);
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1_name"] = array (
  'name' => 'dam_courses_dam_students_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
  'save' => true,
  'id_name' => 'dam_courses_dam_students_1dam_courses_ida',
  'link' => 'dam_courses_dam_students_1',
  'table' => 'dam_courses',
  'module' => 'dam_courses',
  'rname' => 'name',
);
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1dam_courses_ida"] = array (
  'name' => 'dam_courses_dam_students_1dam_courses_ida',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_students_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE',
);
