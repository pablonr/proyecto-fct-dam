<?php
// created: 2022-06-01 18:52:47
$dictionary["dam_students"]["fields"]["dam_students_documents_1"] = array (
  'name' => 'dam_students_documents_1',
  'type' => 'link',
  'relationship' => 'dam_students_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_DAM_STUDENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);
