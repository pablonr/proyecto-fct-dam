<?php
// created: 2022-05-19 06:46:23
$dictionary["dam_students"]["fields"]["dam_students_dam_courses_1"] = array (
  'name' => 'dam_students_dam_courses_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_courses_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_COURSES_TITLE',
);
