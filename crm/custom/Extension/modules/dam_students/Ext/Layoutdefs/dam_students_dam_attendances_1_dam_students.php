<?php
 // created: 2022-05-30 19:21:55
$layout_defs["dam_students"]["subpanel_setup"]['dam_students_dam_attendances_1'] = array (
  'order' => 100,
  'module' => 'dam_attendances',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE',
  'get_subpanel_data' => 'dam_students_dam_attendances_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
