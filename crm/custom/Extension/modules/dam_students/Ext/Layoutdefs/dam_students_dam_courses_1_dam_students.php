<?php
 // created: 2022-05-19 06:46:23
$layout_defs["dam_students"]["subpanel_setup"]['dam_students_dam_courses_1'] = array (
  'order' => 100,
  'module' => 'dam_courses',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_COURSES_TITLE',
  'get_subpanel_data' => 'dam_students_dam_courses_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
