<?php
// created: 2022-04-20 15:47:00
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1"] = array (
  'name' => 'dam_courses_dam_student_requests_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE',
  'id_name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
);
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1_name"] = array (
  'name' => 'dam_courses_dam_student_requests_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE',
  'save' => true,
  'id_name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
  'link' => 'dam_courses_dam_student_requests_1',
  'table' => 'dam_courses',
  'module' => 'dam_courses',
  'rname' => 'name',
);
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1dam_courses_ida"] = array (
  'name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
);
