<?php
// created: 2022-04-20 15:49:59
$mod_strings['LBL_CONTACT_INFORMATION'] = 'Datos del alumno';
$mod_strings['LBL_STUDENT_DNI'] = 'DNI';
$mod_strings['LBL_BIRTHDATE'] = 'Fecha de nacimiento';
$mod_strings['LBL_HOME_PHONE'] = 'Teléfono';
$mod_strings['LBL_PARENTAL_CONSENT'] = 'Consentimiento de los padres';
$mod_strings['LBL_PARENTS_FIRST_NAME'] = 'Nombre (Padre/Madre/Tutor)';
$mod_strings['LBL_PARENTS_LAST_NAME'] = 'Apellidos (Padre/Madre/Tutor)';
$mod_strings['LBL_PARENTS_PHONE'] = 'Numero de teléfono (Padre/Madre/Tutor)';
$mod_strings['LBL_PARENTS_EMAIL'] = 'Correo electrónico (Padre/Madre/Tutor)';
$mod_strings['LBL_EDITVIEW_PANEL1'] = 'Datos familia';
$mod_strings['LBL_QUICKCREATE_PANEL1'] = 'Datos familia';
$mod_strings['LBL_REQUEST_STATUS'] = 'Estado de la peticion';
$mod_strings['LBL_EDITVIEW_PANEL2'] = 'Otros datos';
$mod_strings['LBL_REQUEST_ORIGIN'] = 'Origen de la petición';
$mod_strings['LBL_REPEAT_COURSE'] = 'Repite curso';
$mod_strings['LBL_QUICKCREATE_PANEL2'] = 'Otros datos';
$mod_strings['LBL_OBSERVATIONS'] = 'Observaciones';
$mod_strings['LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE'] = 'Quiere cursar';
