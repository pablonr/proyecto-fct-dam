<?php

$entry_point_registry['external_form'] = array(
    'file' => 'custom/RegistrationForm/index.html',
    'auth' => false,
);

$entry_point_registry['external_form_controller'] = array(
    'file' => 'custom/RegistrationForm/controller.php',
    'auth' => false,
);

$entry_point_registry['dc_api'] = array(
    'file' => 'custom/modules/dam_students/discord_api.php',
    'auth' => false,
);


?>