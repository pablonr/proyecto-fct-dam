<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2022-05-19 06:46:23
$dictionary["dam_courses"]["fields"]["dam_students_dam_courses_1"] = array (
  'name' => 'dam_students_dam_courses_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_courses_1',
  'source' => 'non-db',
  'module' => 'dam_students',
  'bean_name' => 'dam_students',
  'vname' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_STUDENTS_TITLE',
);


// created: 2022-04-22 11:23:31
$dictionary["dam_courses"]["fields"]["dam_courses_dam_students_1"] = array (
  'name' => 'dam_courses_dam_students_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_students_1',
  'source' => 'non-db',
  'module' => 'dam_students',
  'bean_name' => 'dam_students',
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE',
);


// created: 2022-04-20 15:47:00
$dictionary["dam_courses"]["fields"]["dam_courses_dam_student_requests_1"] = array (
  'name' => 'dam_courses_dam_student_requests_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'module' => 'dam_student_requests',
  'bean_name' => 'dam_student_requests',
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
);


 // created: 2022-05-30 17:30:09
$dictionary['dam_courses']['fields']['discord_id_c']['inline_edit']='1';
$dictionary['dam_courses']['fields']['discord_id_c']['labelValue']='Id rol Discord';

 
?>