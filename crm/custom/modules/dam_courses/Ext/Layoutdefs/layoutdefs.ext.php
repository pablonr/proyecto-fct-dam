<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2022-05-19 06:46:23
$layout_defs["dam_courses"]["subpanel_setup"]['dam_students_dam_courses_1'] = array (
  'order' => 100,
  'module' => 'dam_students',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_STUDENTS_TITLE',
  'get_subpanel_data' => 'dam_students_dam_courses_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2022-04-22 11:23:31
$layout_defs["dam_courses"]["subpanel_setup"]['dam_courses_dam_students_1'] = array (
  'order' => 100,
  'module' => 'dam_students',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE',
  'get_subpanel_data' => 'dam_courses_dam_students_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2022-04-20 15:47:00
$layout_defs["dam_courses"]["subpanel_setup"]['dam_courses_dam_student_requests_1'] = array (
  'order' => 100,
  'module' => 'dam_student_requests',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
  'get_subpanel_data' => 'dam_courses_dam_student_requests_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['dam_courses']['subpanel_setup']['dam_students_dam_courses_1']['override_subpanel_name'] = 'dam_courses_subpanel_dam_students_dam_courses_1';


//auto-generated file DO NOT EDIT
$layout_defs['dam_courses']['subpanel_setup']['dam_courses_dam_students_1']['override_subpanel_name'] = 'dam_courses_subpanel_dam_courses_dam_students_1';

?>