
$(document).ready(function () {

    var mapsscript = document.createElement('script');
    mapsscript.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA8KXTaRbrKpIcw5OruT6yDXEC0vdTz7DY&libraries=places&callback=getMapsInput');
    document.head.appendChild(mapsscript);

})

function getMapsInput() {
    let type = $("textarea[id*='address_street']").attr('id').split("_")[0];
    let input = document.getElementById(type + "_address_street");

    initMapInput(input, type);
}

function initMapInput(input, type) {
    let options = {
        componentRestrictions: {
            'country': []
        }
    };

    let autocomplete1 = new google.maps.places.Autocomplete(input, options);

    autocomplete1.addListener('place_changed', function () {

        $('input[id*="' + type + '_address_"], textarea[id*="' + type + '_address_"]').val('');
        console.log($('input[id*="' + type + '_address_"], textarea[id*="' + type + '_address_"]'));

        var place = autocomplete1.getPlace();
        console.log(place.address_components[i]);

        if (place.address_components) {
            var direccion = "";
            var number = "";

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                console.log(place.address_components[i]);
                if (addressType == "route") {
                    var val = place.address_components[i]["long_name"];
                    direccion += val;
                }
                if (addressType == "street_number") {
                    var val = place.address_components[i]["short_name"];
                    number = val;
                }
                if (addressType == "locality") {
                    var val = place.address_components[i]["long_name"];
                    document.getElementById(type + "_address_city").value = val;
                }
                if (addressType == "administrative_area_level_1") {
                    var val = place.address_components[i]["long_name"];
                    document.getElementById(type + "_address_state").value = val;
                }
                if (addressType == "postal_code") {
                    var val = place.address_components[i]["long_name"];
                    document.getElementById(type + "_address_postalcode").value = val;
                }
                if (addressType == "country") {
                    var val = place.address_components[i]["long_name"];
                    document.getElementById(type + "_address_country").value = val;
                }
            }

            $("#" + type + "_address_street").val(direccion + "  " + number);
        }
    });
}