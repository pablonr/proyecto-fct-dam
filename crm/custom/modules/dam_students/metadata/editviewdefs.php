<?php
$module_name = 'dam_students';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'includes' =>
      array(
          0 =>
          array(
              'file' => 'custom/modules/dam_students/scripts/maps.js'
          ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => false,
    ),
    'panels' => 
    array (
      'lbl_contact_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'first_name',
            'customCode' => '{html_options name="salutation" id="salutation" options=$fields.salutation.options selected=$fields.salutation.value}&nbsp;<input name="first_name"  id="first_name" size="25" maxlength="25" type="text" value="{$fields.first_name.value}">',
          ),
          1 => 'last_name',
        ),
        1 => 
        array (
          0 => 'email1',
          1 => 
          array (
            'name' => 'student_dni_c',
            'label' => 'LBL_STUDENT_DNI',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'birthdate_c',
            'label' => 'LBL_BIRTHDATE',
          ),
          1 => 'phone_home',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'parental_consent_c',
            'label' => 'LBL_PARENTAL_CONSENT',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'primary_address_street',
            'hideLabel' => true,
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'primary',
              'rows' => 2,
              'cols' => 30,
              'maxlength' => 150,
            ),
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'parents_first_name_c',
            'label' => 'LBL_PARENTS_FIRST_NAME',
          ),
          1 => 
          array (
            'name' => 'parents_last_name_c',
            'label' => 'LBL_PARENTS_LAST_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'parents_phone_c',
            'label' => 'LBL_PARENTS_PHONE',
          ),
          1 => 
          array (
            'name' => 'parents_email_c',
            'label' => 'LBL_PARENTS_EMAIL',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'observations_c',
            'studio' => 'visible',
            'label' => 'LBL_OBSERVATIONS',
          ),
          1 => 
          array (
            'name' => 'dam_courses_dam_students_1_name',
          ),
        ),
      ),
    ),
  ),
);
;
?>
