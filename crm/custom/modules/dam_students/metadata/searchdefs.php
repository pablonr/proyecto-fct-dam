<?php
$module_name = 'dam_students';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'search_name',
        'label' => 'LBL_NAME',
        'type' => 'name',
      ),
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'first_name' => 
      array (
        'name' => 'first_name',
        'default' => true,
        'width' => '10%',
      ),
      'last_name' => 
      array (
        'name' => 'last_name',
        'default' => true,
        'width' => '10%',
      ),
      'email' => 
      array (
        'name' => 'email',
        'default' => true,
        'width' => '10%',
      ),
      'student_dni_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_STUDENT_DNI',
        'width' => '10%',
        'name' => 'student_dni_c',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'dam_courses_dam_students_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
        'id' => 'DAM_COURSES_DAM_STUDENTS_1DAM_COURSES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'dam_courses_dam_students_1_name',
      ),
      'parents_first_name_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_FIRST_NAME',
        'width' => '10%',
        'name' => 'parents_first_name_c',
      ),
      'parents_last_name_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_LAST_NAME',
        'width' => '10%',
        'name' => 'parents_last_name_c',
      ),
      'parents_email_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_EMAIL',
        'width' => '10%',
        'name' => 'parents_email_c',
      ),
      'parents_phone_c' => 
      array (
        'type' => 'phone',
        'default' => true,
        'label' => 'LBL_PARENTS_PHONE',
        'width' => '10%',
        'name' => 'parents_phone_c',
      ),
      'parental_consent_c' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_PARENTAL_CONSENT',
        'width' => '10%',
        'name' => 'parental_consent_c',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
