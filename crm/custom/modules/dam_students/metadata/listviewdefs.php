<?php
$module_name = 'dam_students';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_NAME',
    'link' => true,
    'orderBy' => 'last_name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
  ),
  'PHONE_HOME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_HOME_PHONE',
    'default' => true,
  ),
  'EMAIL1' => 
  array (
    'width' => '15%',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => true,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => true,
  ),
  'DAM_COURSES_DAM_STUDENTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
    'id' => 'DAM_COURSES_DAM_STUDENTS_1DAM_COURSES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'DATE_MODIFIED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => false,
  ),
  'PARENTAL_CONSENT_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_PARENTAL_CONSENT',
    'width' => '10%',
  ),
  'PARENTS_FIRST_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_FIRST_NAME',
    'width' => '10%',
  ),
  'PARENTS_PHONE_C' => 
  array (
    'type' => 'phone',
    'default' => false,
    'label' => 'LBL_PARENTS_PHONE',
    'width' => '10%',
  ),
  'PARENTS_LAST_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_LAST_NAME',
    'width' => '10%',
  ),
  'STUDENT_DNI_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_STUDENT_DNI',
    'width' => '10%',
  ),
  'PARENTS_EMAIL_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_EMAIL',
    'width' => '10%',
  ),
);
;
?>
