<?php
$popupMeta = array (
    'moduleMain' => 'dam_students',
    'varName' => 'dam_students',
    'orderBy' => 'dam_students.first_name, dam_students.last_name',
    'whereClauses' => array (
  'first_name' => 'dam_students.first_name',
  'last_name' => 'dam_students.last_name',
  'created_by_name' => 'dam_students.created_by_name',
  'email' => 'dam_students.email',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
  2 => 'created_by_name',
  3 => 'email',
),
    'searchdefs' => array (
  'first_name' => 
  array (
    'name' => 'first_name',
    'width' => '10%',
  ),
  'last_name' => 
  array (
    'name' => 'last_name',
    'width' => '10%',
  ),
  'created_by_name' => 
  array (
    'name' => 'created_by_name',
    'width' => '10%',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10%',
  ),
),
);
