<?php

if($_POST['action'] == "register_student") {

    $email = strtolower($_POST['email']);
    $userId = $_POST['user_id'];

    $checkEmailQuery = "SELECT ds.id, dcc.discord_id_c FROM dam_students ds
                        INNER JOIN email_addr_bean_rel eabr ON eabr.bean_id = ds.id
                        INNER JOIN email_addresses ea ON ea.id = eabr.email_address_id
                        INNER JOIN dam_courses_dam_students_1_c dcds ON dcds.dam_courses_dam_students_1dam_students_idb = ds.id
                        INNER JOIN dam_courses dc ON dc.id = dcds.dam_courses_dam_students_1dam_courses_ida
                        INNER JOIN dam_courses_cstm dcc ON dcc.id_c = dc.id
                        WHERE ea.email_address = '$email' AND ds.deleted = 0 AND eabr.deleted = 0 AND ea.deleted = 0 ";
    $resultCheckEmailQuery = $GLOBALS['db']->query($checkEmailQuery);

    if($resultCheckEmailQuery->num_rows == 0) {
        echo json_encode(array(
            "msg" => "El correo introducido no esta registrado en el centro. Si piensas que esto puede ser un error revisa que el correo esté bien escrito o contacta con el centro.",
            "error" => true,
        ));
        exit();
    }

    $checkRegistryQuery = "SELECT ds.id FROM dam_students ds
                            INNER JOIN dam_students_cstm dsc ON dsc.id_c = ds.id
                            INNER JOIN email_addr_bean_rel eabr ON eabr.bean_id = ds.id
                            INNER JOIN email_addresses ea ON ea.id = eabr.email_address_id
                            WHERE ea.email_address = '$email' AND (dsc.discord_id_c IS NOT NULL AND dsc.discord_id_c != '') AND ds.deleted = 0 AND eabr.deleted = 0 AND ea.deleted = 0";
    $resultCheckRegistryQuery = $GLOBALS['db']->query($checkRegistryQuery);

    if($resultCheckRegistryQuery->num_rows > 0) {
        echo json_encode(array(
            "msg" => "Ese correo ya ha sido registrado anteriormente en el servidor. Si piensa que esto se puede tratar de un error contacte con el centro",
            "error" => true
        ));
        exit();
    }

    $checkUserIdQuery = "SELECT dsc.id_c FROM dam_students_cstm dsc 
                        INNER JOIN dam_students ds ON ds.id = dsc.id_c
                        WHERE dsc.discord_id_c = '$userId' AND ds.deleted = 0";
    $resultCheckUserIdQuery = $GLOBALS['db']->query($checkUserIdQuery);
    if($resultCheckUserIdQuery->num_rows > 0) {
        echo json_encode(array(
            "msg" => "Ya te has registrado en el servidor del centro.",
            "error" => true
        ));
        exit();
    }

    $rowCheckEmailQuery = $resultCheckEmailQuery->fetch_assoc();
    $studentId = $rowCheckEmailQuery['id'];
    $roleId = $rowCheckEmailQuery['discord_id_c'];

    $updateQuery = "UPDATE dam_students_cstm SET discord_id_c = '$userId' WHERE id_c = '$studentId'";
    $GLOBALS['db']->query($updateQuery);

    echo json_encode(array(
        "msg" => "Su registro se ha completado correctamente. Ya tienes acceso al servidor",
        "error" => false,
        "roleId" => $roleId
    ));
    exit();
}

if($_POST['action'] == "student_joined") {

    $userId = $_POST['user_id'];

    $getStudentQuery = "SELECT id_c FROM dam_students_cstm dsc
                        INNER JOIN dam_students ds ON ds.id = dsc.id_c 
                        WHERE discord_id_c = '$userId' AND ds.deleted = 0";
    $result = $GLOBALS['db']->query($getStudentQuery);

    if($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        $id = create_guid();
        $insertJoinQuery = "INSERT INTO dam_attendances (id, date_entered, deleted) VALUES ('$id', NOW(), 0)";
        $insertJoinCSTMQuery = "INSERT INTO dam_attendances_cstm (id_c, type_c) VALUES ('$id', 'join')";

        $GLOBALS['db']->query($insertJoinQuery);
        $GLOBALS['db']->query($insertJoinCSTMQuery);

        $relId = create_guid();
        $insertRel = "INSERT INTO dam_students_dam_attendances_1_c VALUES ('$relId', NOW(), 0, '$row[id_c]', '$id')";
        $GLOBALS['db']->query($insertRel);

    }

    echo json_encode(array("error" => false, "msg" => "Join registered for user id: $userId"));
    exit();

}

if($_POST['action'] == "student_left") {

    $userId = $_POST['user_id'];

    $getStudentQuery = "SELECT id_c FROM dam_students_cstm WHERE discord_id_c = '$userId'";
    $result = $GLOBALS['db']->query($getStudentQuery);

    if($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        $id = create_guid();
        $insertLeaveQuery = "INSERT INTO dam_attendances (id, date_entered, deleted) VALUES ('$id', NOW(), 0)";
        $insertLeaveCSTMQuery = "INSERT INTO dam_attendances_cstm (id_c, type_c) VALUES ('$id', 'leave')";

        $GLOBALS['db']->query($insertLeaveQuery);
        $GLOBALS['db']->query($insertLeaveCSTMQuery);

        $relId = create_guid();
        $insertRel = "INSERT INTO dam_students_dam_attendances_1_c VALUES ('$relId', NOW(), 0, '$row[id_c]', '$id')";
        $GLOBALS['db']->query($insertRel);

    }

    echo json_encode(array("error" => false, "msg" => "Leave registered for user id: $userId"));
    exit();
}

if($_POST['action'] == "update_roles") {

    $studentIds = json_decode(htmlspecialchars_decode($_POST['student_ids']));
    $relArray = array();

    foreach($studentIds as $id) {
        $getRelation = "SELECT dsc.discord_id_c AS student_id, dcc.discord_id_c AS course_id FROM dam_students_cstm dsc
                        INNER JOIN dam_students ds ON ds.id = dsc.id_c
                        INNER JOIN dam_courses_dam_students_1_c dcds ON dcds.dam_courses_dam_students_1dam_students_idb = dsc.id_c
                        INNER JOIN dam_courses_cstm dcc ON dcc.id_c = dcds.dam_courses_dam_students_1dam_courses_ida
                        WHERE dsc.discord_id_c = '$id' AND dcds.deleted = 0 AND ds.deleted = 0";
        
        $result = $GLOBALS['db']->query($getRelation);

        if($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $relArray[$row['student_id']] = $row['course_id'];
        }
    }

    echo json_encode(array(
        "msg" => "Los roles se han actualizado correctamente",
        "memberRoleRelation" => $relArray
    ));
    exit();

}

if($_POST['action'] == 'upload_attachment') {

    $path = "custom/modules/dam_students/testAttachment/";

    $getStudentIdQuery = "SELECT dsc.id_c, CONCAT(COALESCE(ds.first_name, ''), ' ', COALESCE(ds.last_name, '')) AS student_name, dc.assigned_user_id AS teacher_id FROM dam_students_cstm dsc
                            INNER JOIN dam_students ds ON ds.id = dsc.id_c
                            INNER JOIN dam_courses_dam_students_1_c dcds ON dcds.dam_courses_dam_students_1dam_students_idb = ds.id
                            INNER JOIN dam_courses dc ON dc.id = dcds.dam_courses_dam_students_1dam_courses_ida
                            WHERE ds.deleted = 0 AND dcds.deleted = 0 AND dc.deleted = 0 AND dsc.discord_id_c = '$_POST[user_id]'";
    $result = $GLOBALS['db']->query($getStudentIdQuery);

    if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        copy($_FILES['file']['tmp_name'], $path . $_FILES['file']['name']);

        $studentId = $row['id_c'];
        $teacherId = $row['teacher_id'];
        $fileId = create_guid();
        $documentId = create_guid();
        $relId = create_guid();
        $studentName = $row['student_name'];
        $documentName = "Entrega de " . $studentName . " del " . date('d-m-Y');
        $fileExtension = end(explode('.', $_FILES['file']['name']));
        $fileMimeType = $_FILES['file']['type'];
        
        rename($path . $_FILES['file']['name'], 'upload/' . $fileId);

        $insertDocument = "INSERT INTO documents (id, date_entered, date_modified, deleted, assigned_user_id, document_name, doc_type, active_date, document_revision_id, is_template) VALUES ('$documentId', NOW(), NOW(), 0, '$teacherId', '$documentName', 'Sugar', NOW(), '$fileId', 0)";
        $insertRevision = "INSERT INTO document_revisions (id, change_log, document_id, doc_type, date_entered, filename, file_ext, file_mime_type, revision, deleted, date_modified, created_by) VALUES ('$fileId', 'Documento Creado', '$documentId', 'Sugar', NOW(), '$documentName.$fileExtension', '$fileExtension', '$fileMimeType', '1', 0, NOW(), '1')";
        $insertStudentRel = "INSERT INTO dam_students_documents_1_c (id, date_modified, deleted, dam_students_documents_1dam_students_ida, dam_students_documents_1documents_idb) VALUES ('$relId', NOW(), 0, '$studentId', '$documentId')";

        $GLOBALS['db']->query($insertDocument);
        $GLOBALS['db']->query($insertRevision);
        $GLOBALS['db']->query($insertStudentRel);

        echo json_encode(array(
            'error' => false,
            'msg' => 'La enterga se ha realizado correctamente'
        ));
        exit();

    } 

    echo json_encode(array(
        'error' => true,
        'msg' => 'Tu usuario no registro no ha sido encontrado. Tu entrega no ha sido regitrada.'
    ));
    exit();
}

?>