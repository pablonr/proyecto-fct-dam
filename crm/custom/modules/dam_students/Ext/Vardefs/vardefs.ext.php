<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2022-05-30 19:21:55
$dictionary["dam_students"]["fields"]["dam_students_dam_attendances_1"] = array (
  'name' => 'dam_students_dam_attendances_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_attendances_1',
  'source' => 'non-db',
  'module' => 'dam_attendances',
  'bean_name' => 'dam_attendances',
  'side' => 'right',
  'vname' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE',
);


// created: 2022-04-22 11:23:31
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1"] = array (
  'name' => 'dam_courses_dam_students_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_students_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
  'id_name' => 'dam_courses_dam_students_1dam_courses_ida',
);
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1_name"] = array (
  'name' => 'dam_courses_dam_students_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_COURSES_TITLE',
  'save' => true,
  'id_name' => 'dam_courses_dam_students_1dam_courses_ida',
  'link' => 'dam_courses_dam_students_1',
  'table' => 'dam_courses',
  'module' => 'dam_courses',
  'rname' => 'name',
);
$dictionary["dam_students"]["fields"]["dam_courses_dam_students_1dam_courses_ida"] = array (
  'name' => 'dam_courses_dam_students_1dam_courses_ida',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_students_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENTS_1_FROM_DAM_STUDENTS_TITLE',
);


// created: 2022-05-19 06:46:23
$dictionary["dam_students"]["fields"]["dam_students_dam_courses_1"] = array (
  'name' => 'dam_students_dam_courses_1',
  'type' => 'link',
  'relationship' => 'dam_students_dam_courses_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_COURSES_TITLE',
);


// created: 2022-06-01 18:52:47
$dictionary["dam_students"]["fields"]["dam_students_documents_1"] = array (
  'name' => 'dam_students_documents_1',
  'type' => 'link',
  'relationship' => 'dam_students_documents_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'side' => 'right',
  'vname' => 'LBL_DAM_STUDENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
);


 // created: 2022-04-22 11:14:10
$dictionary['dam_students']['fields']['parents_email_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['parents_email_c']['labelValue']='Correo electrónico (Padre/Madre/Tutor)';

 

 // created: 2022-04-22 11:14:58
$dictionary['dam_students']['fields']['parents_first_name_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['parents_first_name_c']['labelValue']='Nombre (Padre/Madre/Tutor)';

 

 // created: 2022-05-30 06:54:50
$dictionary['dam_students']['fields']['discord_id_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['discord_id_c']['labelValue']='discord id';

 

 // created: 2022-04-22 11:15:18
$dictionary['dam_students']['fields']['observations_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['observations_c']['labelValue']='Observaciones';

 

 // created: 2022-04-22 11:15:52
$dictionary['dam_students']['fields']['birthdate_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['birthdate_c']['labelValue']='Fecha de nacimiento';

 

 // created: 2022-04-22 11:16:22
$dictionary['dam_students']['fields']['parents_phone_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['parents_phone_c']['labelValue']='Numero de teléfono (Padre/Madre/Tutor)';

 

 // created: 2022-04-22 11:16:49
$dictionary['dam_students']['fields']['parents_last_name_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['parents_last_name_c']['labelValue']='Apellidos (Padre/Madre/Tutor)';

 

 // created: 2022-04-22 11:17:08
$dictionary['dam_students']['fields']['student_dni_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['student_dni_c']['labelValue']='DNI';

 

 // created: 2022-04-22 11:17:32
$dictionary['dam_students']['fields']['parental_consent_c']['inline_edit']='1';
$dictionary['dam_students']['fields']['parental_consent_c']['labelValue']='Consentimiento de los padres';

 
?>