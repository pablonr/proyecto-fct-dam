<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2022-05-30 19:21:55
$layout_defs["dam_students"]["subpanel_setup"]['dam_students_dam_attendances_1'] = array (
  'order' => 100,
  'module' => 'dam_attendances',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DAM_ATTENDANCES_1_FROM_DAM_ATTENDANCES_TITLE',
  'get_subpanel_data' => 'dam_students_dam_attendances_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2022-05-19 06:46:23
$layout_defs["dam_students"]["subpanel_setup"]['dam_students_dam_courses_1'] = array (
  'order' => 100,
  'module' => 'dam_courses',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DAM_COURSES_1_FROM_DAM_COURSES_TITLE',
  'get_subpanel_data' => 'dam_students_dam_courses_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2022-06-01 18:52:47
$layout_defs["dam_students"]["subpanel_setup"]['dam_students_documents_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DAM_STUDENTS_DOCUMENTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'dam_students_documents_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['dam_students']['subpanel_setup']['dam_students_dam_courses_1']['override_subpanel_name'] = 'dam_students_subpanel_dam_students_dam_courses_1';


//auto-generated file DO NOT EDIT
$layout_defs['dam_students']['subpanel_setup']['dam_students_dam_attendances_1']['override_subpanel_name'] = 'dam_students_subpanel_dam_students_dam_attendances_1';

?>