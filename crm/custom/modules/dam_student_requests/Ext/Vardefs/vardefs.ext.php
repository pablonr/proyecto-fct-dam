<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2022-04-20 15:47:00
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1"] = array (
  'name' => 'dam_courses_dam_student_requests_1',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'module' => 'dam_courses',
  'bean_name' => 'dam_courses',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE',
  'id_name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
);
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1_name"] = array (
  'name' => 'dam_courses_dam_student_requests_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE',
  'save' => true,
  'id_name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
  'link' => 'dam_courses_dam_student_requests_1',
  'table' => 'dam_courses',
  'module' => 'dam_courses',
  'rname' => 'name',
);
$dictionary["dam_student_requests"]["fields"]["dam_courses_dam_student_requests_1dam_courses_ida"] = array (
  'name' => 'dam_courses_dam_student_requests_1dam_courses_ida',
  'type' => 'link',
  'relationship' => 'dam_courses_dam_student_requests_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_STUDENT_REQUESTS_TITLE',
);


 // created: 2022-05-09 19:00:28
$dictionary['dam_student_requests']['fields']['request_status_c']['inline_edit']='';
$dictionary['dam_student_requests']['fields']['request_status_c']['labelValue']='Estado de la peticion';

 

 // created: 2022-04-20 15:22:09
$dictionary['dam_student_requests']['fields']['request_origin_c']['inline_edit']='';
$dictionary['dam_student_requests']['fields']['request_origin_c']['labelValue']='Origen de la petición';

 

 // created: 2022-04-19 17:51:07
$dictionary['dam_student_requests']['fields']['parents_email_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['parents_email_c']['labelValue']='Correo electrónico (Padre/Madre/Tutor)';

 

 // created: 2022-04-20 15:27:30
$dictionary['dam_student_requests']['fields']['repeat_course_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['repeat_course_c']['labelValue']='Repite curso';

 

 // created: 2022-04-19 17:49:36
$dictionary['dam_student_requests']['fields']['parents_first_name_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['parents_first_name_c']['labelValue']='Nombre (Padre/Madre/Tutor)';

 

 // created: 2022-04-20 15:39:53
$dictionary['dam_student_requests']['fields']['observations_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['observations_c']['labelValue']='Observaciones';

 

 // created: 2022-04-19 17:14:12
$dictionary['dam_student_requests']['fields']['birthdate_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['birthdate_c']['options']='date_range_search_dom';
$dictionary['dam_student_requests']['fields']['birthdate_c']['labelValue']='Fecha de nacimiento';
$dictionary['dam_student_requests']['fields']['birthdate_c']['enable_range_search']='1';

 

 // created: 2022-04-19 17:50:22
$dictionary['dam_student_requests']['fields']['parents_phone_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['parents_phone_c']['labelValue']='Numero de teléfono (Padre/Madre/Tutor)';

 

 // created: 2022-04-19 17:49:17
$dictionary['dam_student_requests']['fields']['parents_last_name_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['parents_last_name_c']['labelValue']='Apellidos (Padre/Madre/Tutor)';

 

 // created: 2022-04-19 17:12:50
$dictionary['dam_student_requests']['fields']['student_dni_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['student_dni_c']['labelValue']='DNI';

 

 // created: 2022-04-19 17:16:25
$dictionary['dam_student_requests']['fields']['parental_consent_c']['inline_edit']='1';
$dictionary['dam_student_requests']['fields']['parental_consent_c']['labelValue']='Consentimiento de los padres';

 
?>