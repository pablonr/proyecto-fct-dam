$(document).ready(function () {
    $('[id="SAVE"]').removeAttr('onclick').click(function (e) {
        if ($('#request_status_c').val() != 'pending') {
            if (confirm(`¿Seguro que quieres guardar la peticion con el estado "${$('#request_status_c').val()=='accepted' ? 'Aceptada': 'Rechazada'}"? Hacerlo puede generar un nuevo alumno`)) {
                var _form = document.getElementById('EditView'); _form.action.value = 'Save'; if (check_form('EditView')) SUGAR.ajaxUI.submitForm(_form); return false;
            }
        } else {
            var _form = document.getElementById('EditView'); _form.action.value = 'Save'; if (check_form('EditView')) SUGAR.ajaxUI.submitForm(_form); return false;
        }
        e.preventDefault()
    })
})