<?php

class CustomAfterSave {
    /**
     * @param $bean A bean from the student request
     * If the request is accepted checks if a new student has to be generated or update an already existing one.
     */
    function generateStudent($bean, $event, $arguments) {
        if($bean->request_status_c == 'accepted') {
            $studentId = studentExists($bean->email1);

            if($studentId) {
                $studentBean = BeanFactory::getBean('dam_students', $studentId);
                if($studentBean->dam_courses_dam_students_1dam_courses_ida) {
                    $studentBean->load_relationship('dam_students_dam_courses_1');
                    $courseBean = BeanFactory::getBean('dam_courses', $studentBean->dam_courses_dam_students_1dam_courses_ida);
                    $studentBean->dam_students_dam_courses_1->add($courseBean);
                }
                $studentBean->dam_courses_dam_students_1dam_courses_ida = $bean->dam_courses_dam_student_requests_1dam_courses_ida;
                $studentBean->first_name = $bean->first_name;
                $studentBean->last_name = $bean->last_name;
                $studentBean->email1 = $bean->email1;
                $studentBean->student_dni_c = $bean->student_dni_c;
                $studentBean->birthdate_c = $bean->birthdate_c;
                $studentBean->phone_home = $bean->phone_home;
                $studentBean->parental_consent_c = $bean->parental_consent_c;
                $studentBean->primary_address_street = $bean->primary_address_street;
                $studentBean->primary_address_city = $bean->primary_address_city;
                $studentBean->primary_address_state = $bean->primary_address_state;
                $studentBean->primary_address_postalcode = $bean->primary_address_postalcode;
                $studentBean->primary_address_country = $bean->primary_address_country;
                $studentBean->parents_first_name_c = $bean->parents_first_name_c;
                $studentBean->parents_last_name_c = $bean->parents_last_name_c;
                $studentBean->parents_phone_c = $bean->parents_phone_c;
                $studentBean->parents_email_c = $bean->parents_email_c;
                $studentBean->observations_c = $bean->observations_c;
                $studentBean->save();

            } else {
                $studentBean = BeanFactory::newBean('dam_students');
                $studentBean->first_name = $bean->first_name;
                $studentBean->last_name = $bean->last_name;
                $studentBean->email1 = $bean->email1;
                $studentBean->student_dni_c = $bean->student_dni_c;
                $studentBean->birthdate_c = $bean->birthdate_c;
                $studentBean->phone_home = $bean->phone_home;
                $studentBean->parental_consent_c = $bean->parental_consent_c;
                $studentBean->primary_address_street = $bean->primary_address_street;
                $studentBean->primary_address_city = $bean->primary_address_city;
                $studentBean->primary_address_state = $bean->primary_address_state;
                $studentBean->primary_address_postalcode = $bean->primary_address_postalcode;
                $studentBean->primary_address_country = $bean->primary_address_country;
                $studentBean->parents_first_name_c = $bean->parents_first_name_c;
                $studentBean->parents_last_name_c = $bean->parents_last_name_c;
                $studentBean->parents_phone_c = $bean->parents_phone_c;
                $studentBean->parents_email_c = $bean->parents_email_c;
                $studentBean->observations_c = $bean->observations_c;
                $studentBean->dam_courses_dam_students_1dam_courses_ida = $bean->dam_courses_dam_student_requests_1dam_courses_ida;
                $studentBean->save();
            }
        }
    }
}

/**
 * Searches for a student using the email. 
 * @param string $email Email from the student that is searched. Is needed to filter the query
 * @return bool Returns the student id if exits. Else returns false. 
 */
function studentExists($email) {
    $lowerEmail = strtolower($email);
    $getStudentQuery = "SELECT ds.id FROM dam_students ds
                        INNER JOIN email_addr_bean_rel eabr ON eabr.bean_id = ds.id
                        INNER JOIN email_addresses ea ON ea.id = eabr.email_address_id
                        WHERE LOWER(ea.email_address) = '$lowerEmail' AND ds.deleted = 0 AND eabr.deleted = 0 AND ea.deleted = 0";
    $result = $GLOBALS['db']->query($getStudentQuery);
    if($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row['id'];
    }

    return false;
}

?>