<?php
// Do not store anything in this file that is not part of the array or the hook
//version.  This file will be automatically rebuilt in the future.
$hook_version = 1;
$hook_array = Array();
// position, file, function
$hook_array['after_save'] = Array();
$hook_array['after_save'][] = Array(
    77, // Priority
    'Generates a new student if the request is accepted', // Description
    'custom/modules/dam_student_requests/logic_hooks/after_save.php', // Path to the logic hooks file
    'CustomAfterSave', // Class name
    'generateStudent'); // Function name 

?>