<?php
$module_name = 'dam_student_requests';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'search_name',
        'label' => 'LBL_NAME',
        'type' => 'name',
      ),
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
      ),
    ),
    'advanced_search' => 
    array (
      'first_name' => 
      array (
        'name' => 'first_name',
        'default' => true,
        'width' => '10%',
      ),
      'last_name' => 
      array (
        'name' => 'last_name',
        'default' => true,
        'width' => '10%',
      ),
      'created_by_name' => 
      array (
        'name' => 'created_by_name',
        'default' => true,
        'width' => '10%',
      ),
      'email' => 
      array (
        'name' => 'email',
        'default' => true,
        'width' => '10%',
      ),
      'phone_home' => 
      array (
        'type' => 'phone',
        'label' => 'LBL_HOME_PHONE',
        'width' => '10%',
        'default' => true,
        'name' => 'phone_home',
      ),
      'student_dni_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_STUDENT_DNI',
        'width' => '10%',
        'name' => 'student_dni_c',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      'observations_c' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_OBSERVATIONS',
        'sortable' => false,
        'width' => '10%',
        'name' => 'observations_c',
      ),
      'birthdate_c' => 
      array (
        'type' => 'date',
        'default' => true,
        'label' => 'LBL_BIRTHDATE',
        'width' => '10%',
        'name' => 'birthdate_c',
      ),
      'primary_address_street' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRIMARY_ADDRESS_STREET',
        'width' => '10%',
        'default' => true,
        'name' => 'primary_address_street',
      ),
      'primary_address_city' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRIMARY_ADDRESS_CITY',
        'width' => '10%',
        'default' => true,
        'name' => 'primary_address_city',
      ),
      'primary_address_state' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRIMARY_ADDRESS_STATE',
        'width' => '10%',
        'default' => true,
        'name' => 'primary_address_state',
      ),
      'primary_address_postalcode' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
        'width' => '10%',
        'default' => true,
        'name' => 'primary_address_postalcode',
      ),
      'primary_address_country' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_PRIMARY_ADDRESS_COUNTRY',
        'width' => '10%',
        'default' => true,
        'name' => 'primary_address_country',
      ),
      'parents_first_name_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_FIRST_NAME',
        'width' => '10%',
        'name' => 'parents_first_name_c',
      ),
      'parents_last_name_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_LAST_NAME',
        'width' => '10%',
        'name' => 'parents_last_name_c',
      ),
      'parents_phone_c' => 
      array (
        'type' => 'phone',
        'default' => true,
        'label' => 'LBL_PARENTS_PHONE',
        'width' => '10%',
        'name' => 'parents_phone_c',
      ),
      'parents_email_c' => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_PARENTS_EMAIL',
        'width' => '10%',
        'name' => 'parents_email_c',
      ),
      'parental_consent_c' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_PARENTAL_CONSENT',
        'width' => '10%',
        'name' => 'parental_consent_c',
      ),
      'request_status_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_REQUEST_STATUS',
        'width' => '10%',
        'name' => 'request_status_c',
      ),
      'request_origin_c' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_REQUEST_ORIGIN',
        'width' => '10%',
        'name' => 'request_origin_c',
      ),
      'repeat_course_c' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_REPEAT_COURSE',
        'width' => '10%',
        'name' => 'repeat_course_c',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
;
?>
