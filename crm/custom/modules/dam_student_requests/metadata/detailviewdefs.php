<?php
$module_name = 'dam_student_requests';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CONTACT_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_contact_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'first_name',
            'comment' => 'First name of the contact',
            'label' => 'LBL_FIRST_NAME',
          ),
          1 => 
          array (
            'name' => 'last_name',
            'comment' => 'Last name of the contact',
            'label' => 'LBL_LAST_NAME',
          ),
        ),
        1 => 
        array (
          0 => 'email1',
          1 => 
          array (
            'name' => 'student_dni_c',
            'label' => 'LBL_STUDENT_DNI',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'birthdate_c',
            'label' => 'LBL_BIRTHDATE',
          ),
          1 => 
          array (
            'name' => 'phone_home',
            'label' => 'LBL_HOME_PHONE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'parental_consent_c',
            'label' => 'LBL_PARENTAL_CONSENT',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'primary_address_street',
            'label' => 'LBL_PRIMARY_ADDRESS',
            'type' => 'address',
            'displayParams' => 
            array (
              'key' => 'primary',
            ),
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'parents_first_name_c',
            'label' => 'LBL_PARENTS_FIRST_NAME',
          ),
          1 => 
          array (
            'name' => 'parents_last_name_c',
            'label' => 'LBL_PARENTS_LAST_NAME',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'parents_phone_c',
            'label' => 'LBL_PARENTS_PHONE',
          ),
          1 => 
          array (
            'name' => 'parents_email_c',
            'label' => 'LBL_PARENTS_EMAIL',
          ),
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'observations_c',
            'studio' => 'visible',
            'label' => 'LBL_OBSERVATIONS',
          ),
          1 => 
          array (
            'name' => 'dam_courses_dam_student_requests_1_name',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'request_status_c',
            'studio' => 'visible',
            'label' => 'LBL_REQUEST_STATUS',
          ),
          1 => 
          array (
            'name' => 'request_origin_c',
            'studio' => 'visible',
            'label' => 'LBL_REQUEST_ORIGIN',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'repeat_course_c',
            'label' => 'LBL_REPEAT_COURSE',
          ),
          1 => '',
        ),
      ),
    ),
  ),
);
;
?>
