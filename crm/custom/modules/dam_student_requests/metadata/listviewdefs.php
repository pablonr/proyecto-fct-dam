<?php
$module_name = 'dam_student_requests';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_NAME',
    'link' => true,
    'orderBy' => 'last_name',
    'default' => true,
    'related_fields' => 
    array (
      0 => 'first_name',
      1 => 'last_name',
      2 => 'salutation',
    ),
  ),
  'EMAIL1' => 
  array (
    'width' => '15%',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => true,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => true,
  ),
  'DAM_COURSES_DAM_STUDENT_REQUESTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_DAM_COURSES_DAM_STUDENT_REQUESTS_1_FROM_DAM_COURSES_TITLE',
    'id' => 'DAM_COURSES_DAM_STUDENT_REQUESTS_1DAM_COURSES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'REQUEST_STATUS_C' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_REQUEST_STATUS',
    'width' => '10%',
  ),
  'DATE_ENTERED' => 
  array (
    'width' => '10%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
  ),
  'FIRST_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_FIRST_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'LAST_NAME' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LAST_NAME',
    'width' => '10%',
    'default' => false,
  ),
  'PHONE_HOME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_HOME_PHONE',
    'default' => false,
  ),
  'ADDRESS_STREET' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_STREET',
    'default' => false,
  ),
  'ADDRESS_CITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_CITY',
    'default' => false,
  ),
  'ADDRESS_STATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_STATE',
    'default' => false,
  ),
  'ADDRESS_POSTALCODE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_PRIMARY_ADDRESS_POSTALCODE',
    'default' => false,
  ),
  'REQUEST_ORIGIN_C' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_REQUEST_ORIGIN',
    'width' => '10%',
  ),
  'PARENTS_EMAIL_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_EMAIL',
    'width' => '10%',
  ),
  'REPEAT_COURSE_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_REPEAT_COURSE',
    'width' => '10%',
  ),
  'PARENTS_FIRST_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_FIRST_NAME',
    'width' => '10%',
  ),
  'OBSERVATIONS_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_OBSERVATIONS',
    'sortable' => false,
    'width' => '10%',
  ),
  'BIRTHDATE_C' => 
  array (
    'type' => 'date',
    'default' => false,
    'label' => 'LBL_BIRTHDATE',
    'width' => '10%',
  ),
  'PARENTS_PHONE_C' => 
  array (
    'type' => 'phone',
    'default' => false,
    'label' => 'LBL_PARENTS_PHONE',
    'width' => '10%',
  ),
  'PARENTS_LAST_NAME_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_PARENTS_LAST_NAME',
    'width' => '10%',
  ),
  'STUDENT_DNI_C' => 
  array (
    'type' => 'varchar',
    'default' => false,
    'label' => 'LBL_STUDENT_DNI',
    'width' => '10%',
  ),
  'PARENTAL_CONSENT_C' => 
  array (
    'type' => 'bool',
    'default' => false,
    'label' => 'LBL_PARENTAL_CONSENT',
    'width' => '10%',
  ),
);
;
?>
