<?php
$popupMeta = array (
    'moduleMain' => 'dam_student_requests',
    'varName' => 'dam_student_requests',
    'orderBy' => 'dam_student_requests.first_name, dam_student_requests.last_name',
    'whereClauses' => array (
  'first_name' => 'dam_student_requests.first_name',
  'last_name' => 'dam_student_requests.last_name',
  'address_city' => 'dam_student_requests.address_city',
  'created_by_name' => 'dam_student_requests.created_by_name',
  'date_entered' => 'dam_student_requests.date_entered',
  'phone_home' => 'dam_student_requests.phone_home',
  'email' => 'dam_student_requests.email',
),
    'searchInputs' => array (
  0 => 'first_name',
  1 => 'last_name',
  2 => 'address_city',
  3 => 'created_by_name',
  4 => 'date_entered',
  5 => 'phone_home',
  6 => 'email',
),
    'searchdefs' => array (
  'first_name' => 
  array (
    'name' => 'first_name',
    'width' => '10%',
  ),
  'last_name' => 
  array (
    'name' => 'last_name',
    'width' => '10%',
  ),
  'address_city' => 
  array (
    'name' => 'address_city',
    'width' => '10%',
  ),
  'created_by_name' => 
  array (
    'name' => 'created_by_name',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'phone_home' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_HOME_PHONE',
    'width' => '10%',
    'name' => 'phone_home',
  ),
  'email' => 
  array (
    'name' => 'email',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'FULL_NAME' => 
  array (
    'type' => 'fullname',
    'studio' => 
    array (
      'listview' => false,
    ),
    'label' => 'LBL_NAME',
    'width' => '10%',
    'default' => true,
    'name' => 'full_name',
  ),
  'EMAIL1' => 
  array (
    'width' => '15%',
    'label' => 'LBL_EMAIL_ADDRESS',
    'sortable' => false,
    'link' => true,
    'customCode' => '{$EMAIL1_LINK}',
    'default' => true,
    'name' => 'email1',
  ),
  'PHONE_HOME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_HOME_PHONE',
    'default' => true,
    'name' => 'phone_home',
  ),
  'BIRTHDATE_C' => 
  array (
    'type' => 'date',
    'default' => true,
    'label' => 'LBL_BIRTHDATE',
    'width' => '10%',
    'name' => 'birthdate_c',
  ),
),
);
