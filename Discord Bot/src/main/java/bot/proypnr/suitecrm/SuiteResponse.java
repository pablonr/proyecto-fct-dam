package bot.proypnr.suitecrm;

import java.util.Map;

public class SuiteResponse {

    private String msg;
    private boolean error;
    private Long roleId;
    private Map<String, String> memberRoleRelation;

    public String getMsg() {
        return msg;
    }

    public boolean isError() {
        return error;
    }

    public Long getRoleId() {
        return roleId;
    }

    public Map<String, String> getMemberRoleRelation() {
        return memberRoleRelation;
    }
}
