package bot.proypnr.suitecrm;

import bot.proypnr.Config;
import com.google.gson.Gson;
import net.dv8tion.jda.api.entities.*;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class SuiteManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SuiteManager.class);

    private static SuiteManager instance;

    private final String url;

    private SuiteManager() {
        this.url = Config.get("CRM_ENTRYPOINT");
    }

    public static SuiteManager getInstance() {
        if(instance == null) {
           instance = new SuiteManager();
        }

        return instance;
    }

    public void registerStudent(String email, User user, PrivateChannel privateChannel, Guild guild) {

        new Thread(() -> {

            privateChannel.sendTyping().queue();
            StringBuilder bodyContent = new StringBuilder();
            bodyContent.append("action=register_student");
            bodyContent.append("&email=").append(email);
            bodyContent.append("&user_id=").append(user.getId());

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, bodyContent.toString());
            Request request = new Request.Builder()
                    .url(this.url)
                    .method("POST", body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();
            try {
                Response response = client.newCall(request).execute();
                SuiteResponse responseObject = new Gson().fromJson(response.body().string(), SuiteResponse.class);

                privateChannel.sendMessage(responseObject.getMsg()).queue();

                if(!responseObject.isError()) {
                    Role role = guild.getRoleById(responseObject.getRoleId());
                    guild.retrieveMember(user).queue((member) -> {
                        guild.addRoleToMember(member, role).queue();
                    });

                }

                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public void studentJoinVoiceChat(long userId) {
        new Thread(() -> {
            StringBuilder bodyContent = new StringBuilder();
            bodyContent.append("action=student_joined");
            bodyContent.append("&user_id=").append(userId);

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, bodyContent.toString());
            Request request = new Request.Builder()
                    .url(this.url)
                    .method("POST", body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                SuiteResponse responseObject = new Gson().fromJson(response.body().string(), SuiteResponse.class);

                if(!responseObject.isError()) {
                    LOGGER.info(responseObject.getMsg());
                }

                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void studentLeftVoiceChat(long userId) {
        new Thread(() -> {
            StringBuilder bodyContent = new StringBuilder();
            bodyContent.append("action=student_left");
            bodyContent.append("&user_id=").append(userId);

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, bodyContent.toString());
            Request request = new Request.Builder()
                    .url(this.url)
                    .method("POST", body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                SuiteResponse responseObject = new Gson().fromJson(response.body().string(), SuiteResponse.class);

                if(!responseObject.isError()) {
                    LOGGER.info(responseObject.getMsg());
                }

                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    public void updateMemberRoles(long[] memberIds, TextChannel textChannel, Guild guild, Member self) {
        new Thread(() -> {
            Gson gson = new Gson();
            String memberIdsJson = gson.toJson(memberIds);
            StringBuilder bodyContent = new StringBuilder();

            bodyContent.append("action=update_roles");
            bodyContent.append("&student_ids=").append(memberIdsJson);

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, bodyContent.toString());
            Request request = new Request.Builder()
                    .url(this.url)
                    .method("POST", body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                SuiteResponse responseObject = new Gson().fromJson(response.body().string(), SuiteResponse.class);

                Map<String, String> memberRoleRelations = responseObject.getMemberRoleRelation();

                memberRoleRelations.forEach((memberId, roleId) -> {
                    Role role = guild.getRoleById(roleId);

                    guild.retrieveMemberById(memberId).queue((member) -> {
                        member.getRoles().forEach((r) -> {
                            guild.removeRoleFromMember(member, r).queue();
                        });
                    });



                    guild.addRoleToMember(memberId, role).queue();

                });

                textChannel.sendMessage(responseObject.getMsg()).queue();

                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();

    }

    public void sendAttachment(CompletableFuture<File> future, Message message, TextChannel channel, String userId) {
        new Thread(() -> {
            try {
                File file = future.get();
                String mimeType = Files.probeContentType(file.toPath());

                message.delete().queue();

                RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("action", "upload_attachment")
                        .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse(mimeType), file))
                        .addFormDataPart("user_id", userId)
                        .build();

                Request request = new Request.Builder()
                        .url(this.url)
                        .post(requestBody)
                        .build();

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();

                Response response = client.newCall(request).execute();
                SuiteResponse responseObject = new Gson().fromJson(response.body().string(), SuiteResponse.class);

                channel.sendMessage("<@" + userId + "> " + responseObject.getMsg()).queue();

                response.close();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

}
