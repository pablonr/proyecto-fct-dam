package bot.proypnr.command.commands.audios;

import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.lavaplayer.GuildMusicManager;
import bot.proypnr.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

public class StopCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {
        final TextChannel channel = ctx.getChannel();
        final Member selfMember = ctx.getSelfMember();
        final GuildVoiceState selfVoiceState = selfMember.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("Primero tengo que estar en un canal de voz. Puedes usar el comando " + Config.get("PREFIX") +
                    "join para que entre a tu canal de voz").queue();
            return;
        }

        final Member member = ctx.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if(!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("Tienes que estar en un canal de voz para utilizar este comando").queue();
            return;
        }

        if(!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("Tienes que estar en el mismo que el bot para utilizar este comando").queue();
            return;
        }

        final GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(ctx.getGuild());

        musicManager.scheduler.player.stopTrack();
        musicManager.scheduler.queue.clear();

        channel.sendMessage("La cola de musica ha sido vaciada.").queue();
    }

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public String getHelp() {
        return "Para la canción actual y vacía la cola de música.";
    }
}
