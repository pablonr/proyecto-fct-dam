package bot.proypnr.command.commands;

import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.suitecrm.SuiteManager;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class SendCommand implements ICommand {

    SuiteManager suiteManager = SuiteManager.getInstance();

    @Override
    public void handle(CommandContext ctx) {
        Member member = ctx.getMember();
        Message message = ctx.getMessage();
        List<Message.Attachment> attachments = message.getAttachments();
        TextChannel channel = ctx.getChannel();

        if(attachments.isEmpty()){
            channel.sendMessage("Debes añadir un archivo adjunto").queue();
            return;
        }

        Message.Attachment attachment = attachments.get(0);
        String fileName = "entregas/" + attachment.getFileName();
        String fileExtension = attachment.getFileExtension();

        if(!fileExtension.equals("zip") && !fileExtension.equals("png") && !fileExtension.equals("pdf")) {
            channel.sendMessage("Solo se aceptan archivos con extension zip y pdf").queue();
            return;
        }

        CompletableFuture<File> future = attachment.downloadToFile(fileName);
        future.exceptionally(error -> {
            error.printStackTrace();
            return null;
        });

        suiteManager.sendAttachment(future, message, channel, member.getId());
    }

    @Override
    public String getName() {
        return "send";
    }

    @Override
    public String getHelp() {
        return "Envia un archivo como entrega. Debes arrastrar el archivo que deseas enviar y en el comentario añadir el" +
                " comando " + Config.get("PREFIX") + "send";
    }

    @Override
    public List<String> getAliases() {
        return List.of("entregar");
    }
}
