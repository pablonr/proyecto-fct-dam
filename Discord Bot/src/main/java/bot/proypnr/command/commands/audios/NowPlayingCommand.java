package bot.proypnr.command.commands.audios;

import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.lavaplayer.GuildMusicManager;
import bot.proypnr.lavaplayer.PlayerManager;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class NowPlayingCommand implements ICommand {
    @Override
    public void handle(CommandContext ctx) {

        final TextChannel channel = ctx.getChannel();
        final Member selfMember = ctx.getSelfMember();
        final GuildVoiceState selfVoiceState = selfMember.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("Primero tengo que estar en un canal de voz. Puedes usar el comando " + Config.get("PREFIX") +
                    "join para que entre a tu canal de voz").queue();
            return;
        }

        final Member member = ctx.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if(!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("Tienes que estar en un canal de voz para utilizar este comando").queue();
            return;
        }

        if(!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("Tienes que estar en el mismo que el bot para utilizar este comando").queue();
            return;
        }

        final GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(ctx.getGuild());
        final AudioPlayer audioPlayer = musicManager.audioPlayer;

        if(audioPlayer.getPlayingTrack() == null) {
            channel.sendMessage("No esta sonando ninguna canción actualmente").queue();
            return;
        }

        final AudioTrackInfo audioTrackInfo = musicManager.audioPlayer.getPlayingTrack().getInfo();
        final String title = audioTrackInfo.title;
        final String author = audioTrackInfo.author;
        final String url = audioTrackInfo.uri;
        final long durationMS = audioTrackInfo.length;
        final String durationHMS = String.format(
                "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(durationMS),
                TimeUnit.MILLISECONDS.toMinutes(durationMS) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(durationMS) % TimeUnit.MINUTES.toSeconds(1));

        final long currentMS = musicManager.scheduler.player.getPlayingTrack().getPosition();
        final String currentHMS = String.format(
                "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(currentMS),
                TimeUnit.MILLISECONDS.toMinutes(currentMS) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(currentMS) % TimeUnit.MINUTES.toSeconds(1));

        channel.sendMessage("Actualmente está sonando: `")
                .append(title)
                .append("` by `")
                .append(author)
                .append("`. Tiempo: `")
                .append(currentHMS)
                .append("/")
                .append(durationHMS)
                .append("`\n")
                .append("Enlace: ")
                .append(url)
                .queue();

    }

    @Override
    public String getName() {
        return "nowplaying";
    }

    @Override
    public String getHelp() {
        return "Informa sobre la canción sonando acualmente.";
    }

    @Override
    public List<String> getAliases() {
        return List.of("np");
    }
}
