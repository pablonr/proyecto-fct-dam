package bot.proypnr.command.commands.audios;

import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class PlayCommand implements ICommand {

    private final Map<String, String> localAudios = new HashMap<>();

    @SuppressWarnings("ConstantConditions")
    @Override
    public void handle(CommandContext ctx) {
        final TextChannel channel = ctx.getChannel();

        if(ctx.getArgs().isEmpty()) {
            channel.sendMessage("Hace falta un enlace: `" + Config.get("PREFIX") + "play [youtube link]`").queue();
            return;
        }

        final Member selfMember = ctx.getSelfMember();
        final GuildVoiceState selfVoiceState = selfMember.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("Primero tengo que estar en un canal de voz. Puedes usar el comando " + Config.get("PREFIX") +
                    "join para que entre a tu canal de voz").queue();
            return;
        }

        final Member member = ctx.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if(!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("Tienes que estar en un canal de voz para utilizar este comando").queue();
            return;
        }

        if(!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("Tienes que estar en el mismo que el bot para utilizar este comando").queue();
            return;
        }

        String link = String.join(" ", ctx.getArgs());

        if (!isUrl(link)) {
            link = "ytsearch:" + link;
        }

        PlayerManager.getInstance().loadAndPlay(channel, link);

    }

    @Override
    public String getName() {
        return "play";
    }

    @Override
    public String getHelp() {
        return "Reproduce el audio de un video de youtube en un canal de voz. Es necesario entrar a un canal de voz antes.\n" +
                "Ejemplo: `" + Config.get("PREFIX") +  "play [youtube link]`";
    }

    private boolean isUrl(String url) {
        try {
            new URI(url);
            return true;
        } catch (URISyntaxException e) {
            return false;
        }
    }
}
