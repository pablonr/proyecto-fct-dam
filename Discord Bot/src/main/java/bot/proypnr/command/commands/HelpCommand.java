package bot.proypnr.command.commands;

import bot.proypnr.CommandManager;
import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.List;

public class HelpCommand implements ICommand {

    private final CommandManager manager;

    public HelpCommand(CommandManager manager) {
        this.manager = manager;
    }

    @Override
    public void handle(CommandContext ctx) {
        List<String> args = ctx.getArgs();
        TextChannel channel = ctx.getChannel();

        if(args.isEmpty()) {
            StringBuilder builder = new StringBuilder();

            builder.append("Comandos disponibles\n");

            manager.getCommands().stream().map(ICommand::getName).forEach(
                    (it) -> builder.append('`').append(Config.get("PREFIX")).append(it).append("`\n")
            );

            channel.sendMessage(builder.toString()).queue();
            return;
        }

        String search = args.get(0);

        ICommand command = manager.getCommand(search);

        if(command == null) {
            channel.sendMessage("El comando " + search + " no existe").queue();
            return;
        }

        channel.sendMessage(command.getHelp()).queue();

    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getHelp() {
        return "Muestra una lista de los comandos existentes\n" +
                "Ejemplo: `" + Config.get("PREFIX") + "help [command]`";
    }

    @Override
    public List<String> getAliases() {
        return List.of("commands", "cmds", "commandlist", "comandos");
    }
}
