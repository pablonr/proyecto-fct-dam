package bot.proypnr.command.commands;

import bot.proypnr.Config;
import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.suitecrm.SuiteManager;
import net.dv8tion.jda.api.entities.*;

import java.util.List;
import java.util.stream.LongStream;

public class UpdateRolesCommand implements ICommand {

    private SuiteManager suiteManager = SuiteManager.getInstance();

    @Override
    public void handle(CommandContext ctx) {
        Member member = ctx.getMember();
        TextChannel textChannel = ctx.getChannel();
        Guild guild = ctx.getGuild();
        Member self = ctx.getSelfMember();

        if(!member.getId().equals(Config.get("OWNER_ID"))) {
            textChannel.sendMessage("No tienes permisos para utilizar ese comando").queue();
            return;
        }

        long[] memberIds = guild.getMembers().stream().flatMapToLong(i -> {
            if(!i.getId().equals(Config.get("BOT_ID"))) {
                return LongStream.of(i.getIdLong());
            }
            return null;
        }).toArray();

        suiteManager.updateMemberRoles(memberIds, textChannel, guild, self);

    }

    @Override
    public String getName() {
        return "updateroles";
    }

    @Override
    public String getHelp() {
        return "Sincroniza TODOS los roles del servidor con el curso que esté realizando actualmente el alumno.";
    }

    @Override
    public List<String> getAliases() {
        return List.of("ur", "uroles");
    }
}
