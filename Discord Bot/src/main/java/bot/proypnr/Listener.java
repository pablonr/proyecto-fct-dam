package bot.proypnr;

import bot.proypnr.suitecrm.SuiteManager;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Listener extends ListenerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(Listener.class);
    private final CommandManager manager = new CommandManager();
    private final SuiteManager suiteManager = SuiteManager.getInstance();

    @Override
    public void onReady(@NotNull ReadyEvent event) {
        LOGGER.info("{} is ready", event.getJDA().getSelfUser().getAsTag());
    }

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        User user = event.getAuthor();

        if(user.isBot() || event.isWebhookMessage()) {
            return;
        }

        String prefix = Config.get("PREFIX");
        String raw = event.getMessage().getContentRaw();

        if(raw.startsWith(prefix)) {
            this.manager.handle(event);
        }
    }

    @Override
    public void onGuildVoiceJoin(@NotNull GuildVoiceJoinEvent event) {
        User user = event.getMember().getUser();

        if(user.isBot()) {
            return;
        }

        suiteManager.studentJoinVoiceChat(user.getIdLong());
    }

    @Override
    public void onGuildVoiceLeave(@NotNull GuildVoiceLeaveEvent event) {
        User user = event.getMember().getUser();

        if(user.isBot()) {
            return;
        }

        suiteManager.studentLeftVoiceChat(user.getIdLong());

    }

    @Override
    public void onGuildMemberJoin(@NotNull GuildMemberJoinEvent event) {
        User user = event.getUser();
        String msg = "Bienvenido al servidor de Discord del centro ProyPNR. Si todavia no lo has hecho puedes darte de alta" +
                " en el servidor enviando a este chat tu correo electrónico. (Debe ser el mismo correo que " +
                "proporcionaste al centro)";

        if(user.isBot()) {
            return;
        }

        user.openPrivateChannel().queue((channel) -> {
            channel.sendMessage(msg).queue();
        });
    }

    @Override
    public void onPrivateMessageReceived(@NotNull PrivateMessageReceivedEvent event) {
        User user = event.getAuthor();
        PrivateChannel channel = event.getChannel();
        Guild guild = event.getJDA().getGuildById(Config.get("GUILD_ID"));
        String msg = event.getMessage().getContentRaw();
        StringBuilder log = new StringBuilder();

        if(event.getAuthor().isBot()) {
            return;
        }

        if(!EmailChecker.isEmail(msg)) {
            channel.sendMessage("Debes enviar un correo electrónico valido.").queue();
            return;
        }

        suiteManager.registerStudent(msg, user, channel, guild);

    }
}
