package bot.proypnr;

import bot.proypnr.command.CommandContext;
import bot.proypnr.command.ICommand;
import bot.proypnr.command.commands.HelpCommand;
import bot.proypnr.command.commands.PingCommand;
import bot.proypnr.command.commands.UpdateRolesCommand;
import bot.proypnr.command.commands.audios.*;
import bot.proypnr.command.commands.SendCommand;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class CommandManager {
    private final List<ICommand> commands = new ArrayList<>();

    private final String COMMAND_NOT_FOUND_MESSAGE = "Ese comando no existe";

    public CommandManager() {
        addCommand(new HelpCommand(this));
        addCommand(new PingCommand());
        addCommand(new UpdateRolesCommand());
        addCommand(new SendCommand());

        // Lavaplayer commands
        addCommand(new JoinCommand());
        addCommand(new PlayCommand());
        addCommand(new StopCommand());
        addCommand(new SkipCommand());
        addCommand(new NowPlayingCommand());
    }

    private void addCommand(ICommand cmd) {
        if(commandExists(cmd.getName())) {
            throw new IllegalArgumentException("A command with this name is already present");
        }

        commands.add(cmd);
    }

    public List<ICommand> getCommands() {
        return this.commands;
    }

    public ICommand getCommand(String search) {
        String searchLower = search.toLowerCase();

        for(ICommand cmd : this.commands) {
            if(cmd.getName().equals(searchLower) || cmd.getAliases().contains(searchLower)) {
                return cmd;
            }
        }

        return null;
    }

    private boolean commandExists(String name) {
        return this.commands.stream().anyMatch((it) -> it.getName().equalsIgnoreCase(name));
    }

    void handle(GuildMessageReceivedEvent event) {
        String[] split = event.getMessage().getContentRaw()
                .replaceFirst("(?i)" + Pattern.quote(Config.get("PREFIX")), "")
                .split("\\s+");

        String invoke = split[0].toLowerCase();
        ICommand cmd = this.getCommand(invoke);

        if(cmd != null) {
            event.getChannel().sendTyping().queue();
            List<String> args = Arrays.asList(split).subList(1, split.length);

            CommandContext ctx = new CommandContext(event, args);

            cmd.handle(ctx);
        } else {
            event.getChannel().sendMessage(COMMAND_NOT_FOUND_MESSAGE).queue();
        }
    }
}
